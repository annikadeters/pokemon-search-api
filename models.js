const collection = require("./mongo").collection;

exports.getPokemons = function(search) {
  const pokeCollection = collection("pokemons");
  return pokeCollection
    .find({
      name: {
        $regex: search,
        $options: "i"
      }
    })
    .toArray();
};
