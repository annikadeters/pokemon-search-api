const cors = require("cors"); //damit Anfragen von anderen Ports/Quellen erlaubt werden
const express = require("express");
const connect = require("./mongo").connect;

const apiRoutes = require("./api-routes");

const port = process.env.PORT || 8080; //Falls der Port vorgegeben wird, nimm den, ansonsten nimm 8080

const app = express();

app.use(cors());

//hier sagen wir, dass die Sachen aus der api-routes Datei genutzt werden sollen, sobald ein User /pokemons besucht
app.get("/pokemons", (req, res) => apiRoutes.handleGet(req, res));

connect().then(() => {
  console.log("Database connected");
  app.listen(port, function() {
    console.log(`Express server listens on http://localhost:${port}`);
  });
});
