const pokemons = require("./models");

exports.handleGet = (req, res) => {
  pokemons.getPokemons(req.query.search).then(pokemons => {
    const json = JSON.stringify(pokemons);
    res.write(json);
    res.end();
  });
};
