1: Projekt initialisieren (package.json)
2: Module installieren: express cors mongodb
3: cors für alle Requests aktivieren https://expressjs.com/en/resources/middleware/cors.html, damit Anfragen von anderen Ports/Quellen erlaubt werden

4: response.json() nutzen, statt den HTTP header manuell auf applicant/json setzen http://expressjs.com/en/5x/api.html#res.json
5: get endpoint für /pokemons anlegen, der pokemons aus der MongoDB in JSON zurückgibt
6: utils/mongo.js kann kopiert werden
7: Query Parameter search benutzen, um nach Pokemons zu suchen:

{
$regex: search,
    $options: "i" // case insensitive
}
